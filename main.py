from tkinter import *

window = Tk()

listbox1 = Listbox()
for element in ['red', 'blue', 'white', 'black']:
    listbox1.insert(END, element)
listbox1.pack()

listbox2 = Listbox()
for element in [1, 2, 3, 4, 5]:
    listbox2.insert(END, element)
listbox2.pack()

label_for_indexes = Label()
label_for_indexes.pack()

button = Button(text="Get listbox index")
button.pack()


def onselect(event):
    w = event.widget
    # It is good practise to assign widgets relevant and meaningful names not only "selectlist"...
    print("Cursor %s selection: %s" % (w.widgetName, w.curselection()[0]))
    return w.curselection()[0]


def printout(event, list1_selection, list2_selection):
    print("Event: %s" % event)
    # List selections are only some kind of memory link
    print("List 1: %s" % list1_selection)
    print("List 2: %s" % list2_selection)

    # Use cursor selection to access index - be aware of empty selections
    label_for_indexes.configure(text=(listbox1.curselection(), listbox2.curselection(), ))


# These variables are only result of bind (look at documentation): "If FUNC or SEQUENCE is omitted the bound
# function or list of bound events are returned"
listbox1_selection = listbox1.bind('<<ListboxSelect>>', lambda evt: onselect(evt))
listbox2_selection = listbox2.bind('<<ListboxSelect>>', lambda evt: onselect(evt))

l = lambda evt, args=(listbox1_selection, listbox2_selection): printout(evt, listbox1, listbox2)
button.bind('<Button-1>', func=l)

if __name__ == '__main__':
    window.mainloop()
